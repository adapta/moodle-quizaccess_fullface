E-proctoring plugin for Moodle Quiz
Plugin de autenticação via reconhecimento facial para o acesso ao Quiz do Moodle

E-proctoring solution to authentication user with facil biometrics while doing a quiz attempt.
Solução de autenticação por reconhecimento facial enquanto o aluno responde um questionário.

Requirements / Pré-requisitos

You must create an account at http://www.fullface.com.br to use this plugin.
Você precisa criar uma conta em http://www.fullface.com.br pra utilizar esse plugin.

Instalação
-------

Você deve colocar este código no diretório moodle/enrol/pagseguro

Você pode fazer o "git clone" deste repositório:

https://gitlab.com/adapta/moodle-quizaccess_fullface

Ou fazer o download da útlima versão no link:

https://gitlab.com/adapta/moodle-quizaccess_fullface/-/archive/master/moodle-quizaccess_fullface-master.zip

Este plugin também está disponível em:

https://moodle.org/plugins/quizaccess_fullface

Credits and support
-------------------
This plugin was developed by Adapta in partnership with FullFace.
If you have any trouble using this plugin, please, open an issue at https://gitlab.com/adapta/moodle-quizaccess_fullface/-/issues

Esse plugin foi desenvolvido pela Adapta em parceria com a FullFace.
Se você tiver algum problema ou dificuldade no uso desse plugin, por favor, abra uma "issue" em https://gitlab.com/adapta/moodle-quizaccess_fullface/-/issues
