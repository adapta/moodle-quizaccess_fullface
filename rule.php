<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * FULLFace quiz access plugin
 *
 * @package    quizaccess_fullface
 * @copyright  2019 Daniel Neis Araujo <daniel@adapta.online>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/mod/quiz/accessrule/accessrulebase.php');

/**
 * A rule to authenticate user with FULLFace very N minutes.
 *
 * @copyright  2019 Daniel Neis Araujo <daniel@adapta.online>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class quizaccess_fullface extends quiz_access_rule_base {

    public static function make(quiz $quizobj, $timenow, $canignoretimelimits) {
        $cm = $quizobj->get_cm();
        if (is_null($cm)) {
            return null;
        } else if (!self::is_enabled_in_quizid($cm->id)) {
            return null;
        }
        return new self($quizobj, $timenow);
    }

    public static function save_settings($quiz) {
        global $DB;

        if (isset($quiz->fullfaceenabled) && ($quiz->fullfaceenabled == 1) && !self::is_enabled_in_quizid((int)$quiz->coursemodule)) {
            $DB->insert_record('quizaccess_fullface', (object)['quizid' => $quiz->coursemodule]);
        } else {
            $DB->delete_records('quizaccess_fullface', ['quizid' => $quiz->coursemodule]);
        }
    }

    public function description() {
        return get_string('fullfacerule', 'quizaccess_fullface');
    }

    /**
     * Add any fields that this rule requires to the quiz settings form. This
     * method is called from {@link mod_quiz_mod_form::definition()}, while the
     * security seciton is being built.
     * @param mod_quiz_mod_form $quizform the quiz settings form that is being built.
     * @param MoodleQuickForm $mform the wrapped MoodleQuickForm.
     */
    public static function add_settings_form_fields(mod_quiz_mod_form $quizform, MoodleQuickForm $mform) {

        $mform->addElement('advcheckbox', 'fullfaceenabled', get_string('enable', 'quizaccess_fullface'),
            get_string('enabledesc', 'quizaccess_fullface'));
        $cm = $quizform->get_coursemodule();
        if (empty($cm)) {
            $default = false;
        } else {
            $default = self::is_enabled_in_quizid($cm->id);
        }
        $mform->setDefault('fullfaceenabled', $default);
    }

    /**
     * Return true if enabled on given quiz
     *
     * @param quiz $quiz
     * @return bool
     */
    private static function is_enabled_in_quizid(int $quizid): bool {
        global $DB;
        return $DB->record_exists('quizaccess_fullface', ['quizid' => $quizid]);
    }

    /**
     * Whether the user should be blocked from starting a new attempt or continuing
     * an attempt now.
     * @return string false if access should be allowed, a message explaining the
     *      reason if access should be prevented.
     */
    public function prevent_access() {
        global $USER, $PAGE;

        if (is_siteadmin() || !self::is_enabled_in_quizid($this->quiz->id)) {
         //   return false;
        }
        $config = get_config('quizaccess_fullface');
        if (!$config->apiurl) {
            return false;
        }
        $url = $config->apiurl . '/tokapi/token';
        $params = "grant_type=password&username={$config->username}&password={$config->password}&scope=$config->scope";
        $curl = new curl();
        $res = json_decode($curl->post($url, $params));

        $this->log_request($curl, $res);

        if (!isset($res->access_token)) {
            return 'errorconnectingtofullface';// block if any error
        }

        $url = $config->apiurl . '/lstapi/users/list';
        $username = $USER->username;
        $params = (object)[
         'accessToken' => $res->access_token,
         'projectName' => $config->scope,
         'keys' => [ (object)['key' => 'matricula', 'value' => $username] ]
        ];
        $params = json_encode($params);
        $curl->setHeader('Content-Type: application/json');
        $userlist = json_decode($curl->post($url, $params));

        $this->log_request($curl, $userlist);

        if ($userlist) {
            $url = $config->apiurl . '/lstapi/users/lastauthentication';
            $params = (object)[
                'accessToken' => $res->access_token,
                'projectName' => $config->scope,
                'registrationNumber' => $username
            ];
            $lastauth = json_decode($curl->post($url, json_encode($params)));

            $this->log_request($curl, $lastauth);

            $timezone = new DateTimeZone('America/Sao_Paulo');
            $now = new DateTime(null, $timezone);
            $lastauthdatetime = new DateTime($lastauth->lastAuthentication);
            $diff = $lastauthdatetime->diff($now);
            $diffinminutes =
                ($diff->y * 365 * 24 * 60) +
                ($diff->m * 30.4 * 24 * 60) +
                ($diff->d * 24 * 60) +
                ($diff->h * 60) +
                $diff->i;
            // Nunca deixa acessar se o servidor estiver num horário a frente do fullface.
            if ($diff->invert == 1) {
                $diffinminutes = $diffinminutes * -1;
            }
            if ((isset($lastauth->message) && strpos($lastauth->message, 'Nenhuma autenticação realizada') == 0) ||
                isset($lastauth->Message) || ($lastauth->lastAuthentication === 0) ||
                $diffinminutes > $config->time) {

                    $url = clone $PAGE->url;
                    $url->param(($url->get_param('page') ?? 0) + 1);

                    redirect($config->redirecturl .
                             '/Autenticacao?matricula=' . $username .
                             '&projectName=' . $config->scope .
                             '&redirecturl=' . urlencode($url->out(false)));
            }
            return false;
        } else {
            redirect($config->redirecturl .
                     '/Cadastro?matricula=' . $username .
                     '&projectName=' . $config->scope .
                     '&redirecturl=' . urlencode($PAGE->url->out(false)));
        }
    }

    private function log_request($curl, $response) {
        $cm = get_coursemodule_from_instance('quiz', $this->quiz->id);
        $params = array(
            'objectid' => $this->quiz->id,
            'courseid' => $this->quiz->course,
            'context' => context_module::instance($cm->id),
            'other' => array(
                'errno' => $curl->get_errno(),
                'info' => $curl->get_info(),
                'response_headers' => var_export($curl->get_raw_response(), true),
                'response' => var_export($response, true)
            )
        );
        $event = \quizaccess_fullface\event\fullface_request::create($params);
        $event->trigger();
    }
}
